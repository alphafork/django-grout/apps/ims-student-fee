from ims_accounting.serializers import InvoiceSerializer
from ims_base.serializers import BaseModelSerializer
from ims_student_registration.services import get_student_registration_data
from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied, ValidationError
from reusable_models import get_model_from_string

from .models import StudentFee

InstallmentScheme = get_model_from_string("INSTALLMENT_SCHEME")
InstallmentSubscription = get_model_from_string("INSTALLMENT_SUBSCRIPTION")
InstallmentDownPayment = get_model_from_string("INSTALLMENT_DOWN_PAYMENT")
Installment = get_model_from_string("INSTALLMENT")
InstallmentWallet = get_model_from_string("INSTALLMENT_WALLET")
InstallmentDueExtension = get_model_from_string("INSTALLMENT_DUE_EXTENSION")
InstallmentTransaction = get_model_from_string("INSTALLMENT_TRANSACTION")
Invoice = get_model_from_string("INVOICE")
Account = get_model_from_string("ACCOUNT")
StudentRegistration = get_model_from_string("STUDENT_REGISTRATION")


class StudentFeeSerializer(BaseModelSerializer):
    start_date = serializers.DateField(write_only=True)
    exit_date = serializers.DateField(write_only=True, required=False)
    student_fee = serializers.IntegerField(write_only=True)
    down_payment = serializers.IntegerField(write_only=True, required=False, default=0)
    scheme = serializers.PrimaryKeyRelatedField(
        queryset=InstallmentScheme.objects.all(),
        write_only=True,
        required=False,
    )
    subscription_status = serializers.ChoiceField(
        write_only=True,
        choices=InstallmentSubscription.SUBSCRIPTION_STATUS,
        required=False,
    )
    installment_day = serializers.ChoiceField(
        write_only=True,
        choices=[(i, i) for i in range(1, 32)],
        required=False,
    )
    remarks = serializers.CharField(write_only=True, allow_blank=True, required=False)

    class Meta(BaseModelSerializer.Meta):
        model = StudentFee
        excluded_fields = [
            "installment_subscription",
            "is_active",
        ] + BaseModelSerializer.Meta.excluded_fields
        extra_meta = {
            "scheme": {
                "related_model_url": "/installment/installmentscheme",
            }
        }

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        is_admin = self.context["request"].user.groups.filter(name="Admin").exists()
        down_payment = validated_data.get("down_payment", 0)
        scheme = validated_data.get("scheme", None)
        registration = validated_data.get("registration")
        student_fee = validated_data.get("student_fee", 0)
        validation_errors = []
        if self.instance:
            instance_subscription = self.instance.installment_subscription
            instance_down_payment = getattr(
                instance_subscription, "installmentdownpayment"
            )

            if student_fee != instance_subscription.amount:
                validation_errors.append(
                    {"student_fee": "You are not allowed to update this field."}
                )
            if down_payment != instance_down_payment.amount:
                validation_errors.append(
                    {"down_payment": "You are not allowed to update this field."}
                )
            if (scheme != instance_subscription.scheme) and not is_admin:
                validation_errors.append(
                    {"scheme": "You are not allowed to update this field."}
                )
        else:
            if registration.studentfee_set.filter(is_active=True).exists():
                validation_errors.append(
                    {
                        "registration": (
                            "This registration already have an active subscription."
                        )
                    }
                )

        if not scheme and down_payment != student_fee:
            validation_errors.append(
                {
                    "down_payment": (
                        "Down payment do not match total payable."
                        "Did you forget to select scheme?"
                    )
                }
            )
        if validation_errors:
            raise ValidationError(validation_errors)
        return validated_data

    def create(self, validated_data):
        start_date = validated_data.pop("start_date")
        exit_date = validated_data.pop("exit_date", None)
        registration = validated_data.get("registration")
        status = validated_data.pop("subscription_status", None)
        installment_day = validated_data.pop("installment_day", None)
        student_fee = validated_data.pop("student_fee")
        scheme = validated_data.get("scheme", None)
        remarks = validated_data.pop("remarks", "")
        registration.exit_date = exit_date
        registration.remarks = remarks
        registration.save()
        if scheme:
            validated_data.pop("scheme")
        down_payment = validated_data.pop("down_payment", 0)
        subscription = InstallmentSubscription.objects.create(
            amount=student_fee,
            start_date=start_date,
            scheme=scheme,
            subscription_status=status or "ns",
            payment_status="du",
            installment_day=installment_day,
        )
        InstallmentDownPayment.objects.create(
            subscription=subscription,
            amount=down_payment,
            balance_amount=down_payment,
        )
        validated_data["installment_subscription"] = subscription
        validated_data["is_active"] = True
        return super().create(validated_data)

    def update(self, instance, validated_data):
        scheme = validated_data.pop("scheme", None)
        installment_day = validated_data.pop("installment_day", None)
        start_date = validated_data.pop("start_date")
        subscription = instance.installment_subscription
        exit_date = validated_data.pop("exit_date", None)
        registration = validated_data.get("registration")
        remarks = validated_data.pop("remarks", "")
        registration.exit_date = exit_date
        registration.remarks = remarks
        registration.save()
        if scheme != subscription.scheme:
            subscription.subscription_status = "mi"
            new_subscription = InstallmentSubscription.objects.create(
                amount=subscription.get_current_due_data()[1],
                start_date=start_date,
                scheme=scheme,
                subscription_status="ac",
                payment_status="du",
                installment_day=installment_day,
            )
            validated_data["subscription"] = new_subscription
        else:
            subscription_status = validated_data.pop("subscription_status", None)
            subscription.installment_day = installment_day
            subscription.start_date = start_date
            subscription.subscription_status = subscription_status
        subscription.save()
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        registration = instance.registration
        subscription = instance.installment_subscription
        down_payment = getattr(subscription, "installmentdownpayment", None)
        scheme = getattr(subscription, "scheme", None)
        data.update(
            {
                "start_date": subscription.start_date,
                "exit_date": registration.exit_date,
                "installment_day": subscription.installment_day,
                "student_fee": subscription.amount,
                "is_active": instance.is_active,
                "scheme": subscription.scheme.id if scheme else None,
                "scheme_name": f"{subscription.scheme}" if scheme else None,
                "subscription": f"{subscription}",
                "down_payment": down_payment.amount if down_payment else 0,
                **get_student_registration_data(registration),
            }
        )

        data["due_date"], data["due_amount"] = subscription.get_current_due_data()
        data["subscription_status"] = dict(subscription.SUBSCRIPTION_STATUS)[
            subscription.subscription_status
        ]
        data["payment_status"] = dict(subscription.PAYMENT_STATUS)[
            subscription.payment_status
        ]
        return data


class StudentFeePaymentSerializer(serializers.Serializer):
    invoice = serializers.PrimaryKeyRelatedField(
        write_only=True,
        queryset=Invoice.objects.all(),
    )
    registration = serializers.PrimaryKeyRelatedField(
        write_only=True,
        queryset=StudentRegistration.objects.all(),
    )

    class Meta(BaseModelSerializer.Meta):
        fields = ["invoice", "registration"]

    def create(self, validated_data):
        invoice = validated_data.pop("invoice")
        registration = validated_data.pop("registration")

        student_fee = StudentFee.objects.get(registration=registration, is_active=True)
        subscription = student_fee.installment_subscription
        InstallmentTransaction.objects.create(
            subscription=subscription,
            amount=invoice.amount,
            action="wp",
            transaction=invoice.transaction,
        )

        subscription.pay(
            wallet_payment_amount=invoice.amount,
        )
        return student_fee

    def to_representation(self, instance):
        registration = instance.registration
        subscription = instance.installment_subscription
        data = {
            "subscription": f"{subscription}",
            **get_student_registration_data(registration),
        }
        data["due_date"], data["due_amount"] = subscription.get_current_due_data()
        data["subscription_status"] = dict(subscription.SUBSCRIPTION_STATUS)[
            subscription.subscription_status
        ]
        data["payment_status"] = dict(subscription.PAYMENT_STATUS)[
            subscription.payment_status
        ]
        return data


class StudentFeeReadonlySerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        model = InstallmentSubscription

    def to_representation(self, instance):
        data = super().to_representation(instance)
        registration = StudentFee.objects.get(
            installment_subscription=instance
        ).registration
        data.update(get_student_registration_data(registration))
        data["subscription_status"] = dict(instance.SUBSCRIPTION_STATUS)[
            instance.subscription_status
        ]
        data["payment_status"] = dict(instance.PAYMENT_STATUS)[instance.payment_status]
        data["due_date"], data["due_amount"] = instance.get_current_due_data()
        return data


class StudentUpdateSubscriptionSerializer(BaseModelSerializer):
    start_date = serializers.DateField(write_only=True)
    exit_date = serializers.DateField(write_only=True, required=False)
    scheme = serializers.PrimaryKeyRelatedField(
        queryset=InstallmentScheme.objects.all(),
        write_only=True,
        required=False,
        allow_null=True,
    )
    subscription_status = serializers.ChoiceField(
        write_only=True,
        choices=InstallmentSubscription.SUBSCRIPTION_STATUS,
        required=False,
    )
    installment_day = serializers.ChoiceField(
        write_only=True,
        choices=[(i, i) for i in range(1, 32)],
        required=False,
    )

    class Meta(BaseModelSerializer.Meta):
        model = StudentFee
        excluded_fields = [
            "installment_subscription",
            "registration",
            "is_active",
        ] + BaseModelSerializer.Meta.excluded_fields
        extra_meta = {
            "scheme": {
                "related_model_url": "/installment/installmentscheme",
            }
        }

    def create(self, validated_data):
        raise PermissionDenied

    def update(self, instance, validated_data):
        scheme = validated_data.pop("scheme", None)
        installment_day = validated_data.pop("installment_day", None)
        start_date = validated_data.pop("start_date")
        exit_date = validated_data.pop("exit_date", None)
        registration = instance.registration
        registration.exit_date = exit_date
        registration.save()
        subscription = instance.installment_subscription
        if scheme != subscription.scheme:
            subscription.subscription_status = "mi"
            subscription.save()
            instance.is_active = False
            instance.save()
            new_subscription_amount = (
                subscription.amount - subscription.get_total_paid()
            )
            new_subscription = InstallmentSubscription.objects.create(
                amount=new_subscription_amount,
                start_date=start_date,
                scheme=scheme,
                subscription_status="ac",
                payment_status="du",
                installment_day=installment_day,
            )
            wallet = getattr(subscription, "installmentwallet", None)
            if wallet:
                InstallmentWallet.objects.create(
                    subscription=new_subscription,
                    amount=wallet.amount,
                )
            InstallmentDownPayment.objects.create(
                subscription=new_subscription,
                amount=subscription.get_current_due_data()[1],
                balance_amount=subscription.get_current_due_data()[1],
            )
            student_fee = StudentFee.objects.create(
                registration=instance.registration,
                installment_subscription=new_subscription,
                is_active=True,
            )
            return student_fee
        else:
            subscription_status = validated_data.pop("subscription_status", None)
            subscription.installment_day = installment_day
            subscription.start_date = start_date
            subscription.subscription_status = subscription_status
            subscription.save()
            return super().update(instance, validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        subscription = instance.installment_subscription
        scheme = getattr(subscription, "scheme", None)
        data.update(
            {
                "start_date": subscription.start_date,
                "exit_date": instance.registration.exit_date,
                "installment_day": subscription.installment_day,
                "scheme": subscription.scheme.id if scheme else None,
                "scheme_name": f"{subscription.scheme}" if scheme else None,
                "subscription": f"{subscription}",
                "subscription_status": subscription.subscription_status,
            }
        )
        return data


class StudentInstallmentDueExtensionSerializer(BaseModelSerializer):
    registration = serializers.PrimaryKeyRelatedField(
        write_only=True,
        queryset=StudentRegistration.objects.filter(
            id__in=StudentFee.objects.filter(is_active=True).values("registration")
        ),
    )

    class Meta(BaseModelSerializer.Meta):
        model = InstallmentDueExtension
        excluded_fields = BaseModelSerializer.Meta.excluded_fields + [
            "is_last",
            "subscription",
        ]

    def create(self, validated_data):
        validated_data["is_last"] = True
        registration = validated_data.pop("registration", None)
        subscription = StudentFee.objects.get(
            is_active=True,
            registration=registration,
        ).installment_subscription
        InstallmentDueExtension.objects.filter(subscription=subscription).update(
            is_last=False
        )
        validated_data["subscription"] = subscription
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        registration = StudentFee.objects.get(
            is_active=True,
            installment_subscription=instance.subscription,
        ).registration
        return {
            **data,
            **get_student_registration_data(registration),
            **{"subscription": instance.subscription.id},
        }


class StudentFeeTimelineSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        model = StudentRegistration

    def to_representation(self, instance):
        student_fee = StudentFee.objects.filter(
            registration=instance,
            is_active=True,
        )
        if student_fee:
            subscription = student_fee.get().installment_subscription
            wallet = getattr(subscription, "installmentwallet", None)

            total_balance = subscription.current_balance()
            written_off_amount = 0
            off_installment_payments_amount = 0
            if subscription.subscription_status == "cl":
                written_off = subscription.installmenttransaction_set.filter(
                    action="wo"
                )
                if written_off.exists():
                    written_off_amount = written_off.get().amount
                    off_installment_payments_amount = total_balance - written_off_amount

            timeline = {
                "subscription_status": dict(subscription.SUBSCRIPTION_STATUS)[
                    subscription.subscription_status
                ],
                "payment_status": dict(subscription.PAYMENT_STATUS)[
                    subscription.payment_status
                ],
                "wallet_balance": wallet.amount if wallet else 0,
                "start_date": subscription.start_date,
                "due_date": subscription.get_current_due_data()[0],
                "total_amount": subscription.amount,
                "total_paid": subscription.get_total_paid(),
                "current_payable": subscription.get_current_due_data()[1],
                "payment_timeline": subscription.get_payment_timeline(),
                "written_off_amount": written_off_amount,
                "off_installment_payments_amount": off_installment_payments_amount,
            }
        else:
            timeline = {
                "subscription_status": None,
                "payment_status": None,
                "start_date": None,
                "due_date": None,
                "total_amount": None,
                "total_paid": None,
                "current_payable": None,
                "payment_timeline": None,
                "written_off_amount": None,
                "off_installment_payments_amount": None,
            }
        return {**get_student_registration_data(instance), **timeline}


class FeeInvoiceSerializer(InvoiceSerializer):
    class Meta(InvoiceSerializer.Meta):
        excluded_fields = InvoiceSerializer.Meta.excluded_fields + ["account"]

    def create(self, validated_data):
        validated_data["account"] = Account.objects.get(name="installment wallet")
        return super().create(validated_data)
