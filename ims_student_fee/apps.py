from django.apps import AppConfig


class IMSStudentFeeConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_student_fee"

    model_strings = {
        "STUDENT_FEE": "StudentFee",
    }
