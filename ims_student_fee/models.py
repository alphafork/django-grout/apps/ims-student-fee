from django.db import models
from django.db.models.base import post_save
from django.dispatch import receiver
from ims_base.models import AbstractLog
from ims_notification.services import add_group_notification
from reusable_models import get_model_from_string

StudentRegistration = get_model_from_string("STUDENT_REGISTRATION")
InstallmentSubscription = get_model_from_string("INSTALLMENT_SUBSCRIPTION")
InstallmentDueExtension = get_model_from_string("INSTALLMENT_DUE_EXTENSION")
UserNotification = get_model_from_string("USER_NOTIFICATION")
GroupNotification = get_model_from_string("GROUP_NOTIFICATION")


class StudentFee(AbstractLog):
    registration = models.ForeignKey(StudentRegistration, on_delete=models.CASCADE)
    installment_subscription = models.ForeignKey(
        InstallmentSubscription, on_delete=models.CASCADE
    )
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return self.registration.__str__()


@receiver(post_save, sender=InstallmentSubscription)
def send_overdue_extension_notification(instance, created, **kwargs):
    if instance.payment_status in ["od", "ex"]:
        student_fee = StudentFee.objects.filter(
            installment_subscription=instance,
            is_active=True,
        )
        if student_fee.exists():
            registration = student_fee.get().registration

            if instance.payment_status == "od":
                due_date = instance.get_current_due_data()[0]
                notification = f"{registration} is overdue on {due_date}."
            else:
                extended_date = InstallmentDueExtension.objects.get(
                    subscription=instance,
                    is_last=True,
                )
                notification = (
                    f"{registration} extension date is set to {extended_date}."
                )
            add_group_notification(notification, ["Admin", "Accountant", "Manager"])
            user = registration.student.user
            UserNotification.objects.create(user=user, notification=notification)
