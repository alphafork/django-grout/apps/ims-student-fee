from django.urls import include, path
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from ims_student_fee.apis import StudentFeeReadonlyViewSet

from .apis import (
    FeeInvoiceAPI,
    StudentFeeCloseSubscriptionAPI,
    StudentFeePaymentAPI,
    StudentFeeSplitupAPI,
    StudentFeeStatusAPI,
    StudentFeeTimelineAPI,
    StudentInstallmentDueExtensionAPI,
    StudentStartSubscriptionAPI,
    StudentUpdateSubscriptionAPI,
)

router = routers.SimpleRouter()
router.register(r"installment", StudentFeeReadonlyViewSet, "student-fee-readonly")
router.register(r"payment", StudentFeePaymentAPI, "student-fee-payment")
router.register(
    r"update-subscription",
    StudentUpdateSubscriptionAPI,
    "student-fee-update-subscription",
)
router.register(
    r"due-extension",
    StudentInstallmentDueExtensionAPI,
    "due-extension",
)
router.register(
    r"timeline",
    StudentFeeTimelineAPI,
    "timeline",
)
router.register(
    r"invoice",
    FeeInvoiceAPI,
    "invoice",
)


urlpatterns = [
    path("splitup/<int:pk>/", StudentFeeSplitupAPI.as_view()),
    path("start-installment/<int:pk>/", StudentStartSubscriptionAPI.as_view()),
    path("status/<int:pk>/", StudentFeeStatusAPI.as_view()),
    path("close-subscription/<int:pk>/", StudentFeeCloseSubscriptionAPI.as_view()),
    path("", include(router.urls)),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json"])
