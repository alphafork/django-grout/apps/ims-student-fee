import datetime

from ims_accounting.apis import InvoiceAPI
from ims_api_permission.apis import APIMethodPermission
from ims_base.apis import BaseAPIView, BaseAPIViewSet, BaseRelatedFieldViewSet
from ims_user.apis import UserFilter
from rest_framework.exceptions import NotFound
from rest_framework.filters import BaseFilterBackend
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet
from reusable_models import get_model_from_string

from .models import StudentFee
from .serializers import (
    FeeInvoiceSerializer,
    StudentFeePaymentSerializer,
    StudentFeeReadonlySerializer,
    StudentFeeTimelineSerializer,
    StudentInstallmentDueExtensionSerializer,
    StudentUpdateSubscriptionSerializer,
)

Installment = get_model_from_string("INSTALLMENT")
InstallmentSubscription = get_model_from_string("INSTALLMENT_SUBSCRIPTION")
InstallmentTransaction = get_model_from_string("INSTALLMENT_TRANSACTION")
InstallmentDueExtension = get_model_from_string("INSTALLMENT_DUE_EXTENSION")
Invoice = get_model_from_string("INVOICE")
Course = get_model_from_string("COURSE")
StudentRegistration = get_model_from_string("STUDENT_REGISTRATION")


class StudentDueExtensionFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if "registration_id" in request.query_params:
            subscriptions = StudentFee.objects.filter(
                registration_id=request.query_params["registration_id"]
            ).values("installment_subscription")
            filter_map = {"subscription__in": subscriptions}
            if "is_last" in request.query_params:
                filter_map.update({"is_last": request.query_params["is_last"]})
            return queryset.filter(**filter_map)
        return queryset


class StudentFeeTimelineFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if "student_id" in request.query_params:
            return queryset.filter(student=request.query_params["student_id"])
        return queryset


class StudentFeePaymentAPI(BaseAPIViewSet):
    model_class = StudentFee
    queryset = StudentFee.objects.filter(is_active=True)
    serializer_class = StudentFeePaymentSerializer
    managed_permissions = {
        "OPTIONS": ["Admin", "Accountant"],
        "GET": ["Admin", "Accountant"],
        "POST": ["Admin", "Accountant"],
        "PUT": ["Admin"],
        "PATCH": ["Admin"],
        "DELETE": ["Admin"],
    }
    permission_classes = BaseAPIViewSet.permission_classes + [APIMethodPermission]


class StudentFeeSplitupAPI(BaseAPIView):
    def get(self, request, **kwargs):
        subscription = get_object_or_404(
            StudentFee.objects.filter(is_active=True),
            registration=self.kwargs.get("pk"),
        ).installment_subscription
        installments = Installment.objects.filter(subscription=subscription)
        down_payment = None
        data = {}
        payment_history = []
        total_payable = 0
        if hasattr(subscription, "installmentdownpayment"):
            down_payment = subscription.installmentdownpayment

            payment_history.append(
                {
                    "item": "down payment",
                    "due_date": subscription.start_date,
                    "amount": down_payment.amount,
                    "balance_amount": down_payment.balance_amount,
                }
            )
            total_payable += down_payment.balance_amount

        for installment in installments:
            payment_history.append(
                {
                    "item": "installment " + str(installment.number.number),
                    "due_date": installment.due_date,
                    "amount": installment.amount,
                    "balance_amount": installment.balance_amount,
                }
            )
            if installment.balance_amount > 0:
                total_payable += installment.balance_amount

        data["subscription_status"] = dict(subscription.SUBSCRIPTION_STATUS)[
            subscription.subscription_status
        ]
        data["payment_status"] = dict(subscription.PAYMENT_STATUS)[
            subscription.payment_status
        ]
        data["payment_history"] = payment_history
        data["total_payable"] = total_payable
        return Response(
            status=200,
            data=data,
        )


class StudentFeeTimelineAPI(BaseAPIViewSet):
    http_method_names = ["get"]
    serializer_class = StudentFeeTimelineSerializer
    queryset = StudentRegistration.objects.all()
    filter_backends = BaseAPIViewSet.filter_backends + [
        UserFilter,
        StudentFeeTimelineFilter,
    ]
    user_lookup_field = "student"
    model_class = StudentRegistration
    obj_user_groups = ["Admin", "Accountant", "Manager"]


class StudentFeeReadonlyViewSet(BaseAPIView, ReadOnlyModelViewSet):
    serializer_class = StudentFeeReadonlySerializer
    queryset = InstallmentSubscription.objects.all()

    def get_queryset(self):
        subscription = StudentFee.objects.filter(is_active=True).values_list(
            "installment_subscription__id", flat=True
        )
        return super().get_queryset().filter(pk__in=subscription)


class StudentFeeStatusAPI(BaseAPIView):
    managed_permissions = {
        "GET": ["Admin", "Accountant", "Manager"],
    }

    def get(self, request, **kwargs):
        subscription = get_object_or_404(
            StudentFee.objects.filter(is_active=True),
            registration=kwargs.get("pk"),
        ).installment_subscription
        data = {
            "subscription_status": dict(subscription.SUBSCRIPTION_STATUS)[
                subscription.subscription_status
            ],
            "payment_status": dict(subscription.PAYMENT_STATUS)[
                subscription.payment_status
            ],
        }
        return Response(
            data=data,
            status=200,
        )


class StudentStartSubscriptionAPI(BaseAPIView):
    managed_permissions = {
        "GET": ["Admin", "Accountant"],
    }

    def get(self, request, **kwargs):
        subscription = get_object_or_404(
            StudentFee.objects.filter(is_active=True),
            registration=kwargs.get("pk"),
        ).installment_subscription
        subscription.subscription_status = "ac"
        subscription.start_date = datetime.date.today()
        subscription.save()
        return Response(
            status=200,
        )


class StudentFeeCloseSubscriptionAPI(BaseAPIView):
    managed_permissions = {
        "GET": ["Admin", "Accountant"],
    }

    def get(self, request, **kwargs):
        subscription = get_object_or_404(
            StudentFee.objects.filter(is_active=True),
            registration=kwargs.get("pk"),
        ).installment_subscription
        subscription.close_subscription()

        return Response(
            status=200,
        )


class StudentUpdateSubscriptionAPI(BaseAPIViewSet):
    model_class = StudentFee
    serializer_class = StudentUpdateSubscriptionSerializer
    permission_classes = BaseAPIViewSet.permission_classes + [APIMethodPermission]
    managed_permissions = {
        "OPTIONS": ["Admin", "Accountant"],
        "GET": ["Admin", "Accountant"],
        "PUT": ["Admin", "Accountant"],
        "PATCH": ["Admin", "Accountant"],
    }

    def get_queryset(self):
        subscriptions = StudentFee.objects.filter(is_active=True).values(
            "installment_subscription"
        )
        return super().get_queryset().filter(installment_subscription__in=subscriptions)

    def list(self, request, *args, **kwargs):
        raise NotFound

    def create(self, request, *args, **kwargs):
        raise NotFound

    def destroy(self, request, *args, **kwargs):
        raise NotFound


class StudentInstallmentDueExtensionAPI(BaseRelatedFieldViewSet):
    model_class = InstallmentDueExtension
    serializer_class = StudentInstallmentDueExtensionSerializer
    filter_backends = [StudentDueExtensionFilter]
    permission_classes = BaseAPIViewSet.permission_classes + [APIMethodPermission]
    managed_permissions = {
        "OPTIONS": ["Admin", "Accountant"],
        "GET": ["Admin", "Accountant"],
        "POST": ["Admin", "Accountant"],
        "PUT": ["Admin"],
        "PATCH": ["Admin"],
        "DELETE": ["Admin"],
    }

    def get_queryset(self):
        subscriptions = StudentFee.objects.filter(is_active=True).values(
            "installment_subscription"
        )
        return super().get_queryset().filter(subscription__in=subscriptions)


class FeeInvoiceAPI(InvoiceAPI):
    serializer_class = FeeInvoiceSerializer
    queryset = Invoice.objects.all()
    model_class = Invoice
